{if $DOCUMENT.doc_id == 6}
	{include file='templates/content.blog.php'}
{elseif $DOCUMENT.doc_id == 7}
	{include file='templates/content.blog_post.php'}
{else}
<!--breadcrumb-section starts-->
<div class="breadcrumb-section">
    <div class="container">
        <h1>{$DOCUMENT.doc_name}</h1>
    </div>
</div>
<div class="container">
    <div class="breadcrumb">
            <a href="{$STORE_URL}">Home</a>
        <span class="fa fa-angle-double-right"></span>
        <span class="current">{$DOCUMENT.doc_name}</span>
    </div>
</div>
<!--breadcrumb-section ends-->

<section id="primary" class="content-full-width">
    <div class="container">
    	<div class="document-content">
            <div class="row">
               <div class="small-12 columns">{$DOCUMENT.doc_content}</div>
            </div>
            {if $SHARE}
            <hr>
            <div class="row">
               <div class="small-12 columns">
                  {foreach from=$SHARE item=html}
                  {$html}
                  {/foreach}
               </div>
            </div>
            {/if}
            {foreach from=$COMMENTS item=html}
            {$html}
            {/foreach}
        </div>
    </div>
</section>
{/if}
