{*
 * CubeCart v6
 * ========================================
 * CubeCart is a registered trade mark of CubeCart Limited
 * Copyright CubeCart Limited 2015. All rights reserved.
 * UK Private Limited Company No. 5323904
 * ========================================
 * Web:   http://www.cubecart.com
 * Email:  sales@cubecart.com
 * License:  GPL-3.0 https://www.gnu.org/licenses/quick-guide-gplv3.html
 *}
<div class="search">
    <div class="search_box">
        <form action="{$STORE_URL}/search.html" method="get" id="search">
            <div class="right">
                <button class="search-button" type="submit" value="{$LANG.common.search}"><img src="{$STORE_URL}/skins/{$SKIN_FOLDER}/images/site/magnifier.png" alt="Search"></button>
            </div>
            <div class="left">
                <input name="search[keywords]" type="text" title="Search our store" class="textbox" placeholder="Search our store">
                <input type="hidden" name="_a" value="category">
            </div>
        </form>
    </div>
</div>