<!--breadcrumb-section starts-->
<div class="breadcrumb-section">
    <div class="container">
        <h1>Blog</h1>
    </div>
</div>
<div class="container">
    <div class="breadcrumb">
        <a href="{$STORE_URL}">Home</a>
        <span class="fa fa-angle-double-right"></span>
        <span class="current">Blog</span>
    </div>
</div>
<!--breadcrumb-section ends-->

<section id="primary" class="content-full-width">
  <div class="container">
        <article class="blog-entry">
            <div class="blog-entry-inner">		
                <div class="entry-details">	
                    <div class="entry-title">
                        <h3><a href="blog-detail.html"> Activities Improves Mind </a></h3>
                        <p>27 August 2015</p>
                    </div>			
                    <!--entry-metadata ends-->	
                    <div class="entry-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae tellus at risus mollis pellentesque ac ac elit. Morbi pretium ante dolor, at feugiat enim posuere id. Duis varius erat non lectus rhoncus interdum. Curabitur maximus nec turpis a commodo. Maecenas sit amet felis eu erat tempus cursus. Nullam convallis dictum lorem, at ornare erat efficitur et. Cras tempor augue vitae urna imperdiet, nec faucibus nisi scelerisque.</p>
                        
                        <img src="http://placehold.it/510x300" alt="" title="">

                        <p>Sed est tellus, viverra molestie maximus id, fermentum ut ligula. Suspendisse tincidunt, lorem eu sagittis condimentum, magna sapien bibendum ante, ut consequat diam nibh quis diam. Cras a sollicitudin ex, ac tincidunt eros. Curabitur finibus enim et augue congue, ut semper ipsum lobortis. Cras dui libero, dapibus eu faucibus id, ornare suscipit lectus. Sed consequat consequat justo sit amet condimentum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris vel quam id metus sagittis sagittis. Ut at lorem sem.</p>
                                                            
                        <p>Phasellus suscipit augue vitae urna sagittis dictum. Quisque pretium lacus sit amet pharetra auctor. Morbi sit amet neque et velit feugiat dignissim. Morbi purus ipsum, consequat et metus gravida, pellentesque sollicitudin arcu. Nulla ac nisl scelerisque, elementum dui nec, aliquet ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam volutpat volutpat nisl ac vehicula. Nulla odio augue, scelerisque eu metus nec, cursus euismod erat. Aliquam congue augue eget odio ultrices feugiat et sit amet ligula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                    </div>	 		
                    <a href="blog-detail.html" class="dt-sc-button small"><span class="fa fa-chevron-circle-left"> </span> Back</a>		
                </div>	
            </div>
        </article>
  </div>
</section>