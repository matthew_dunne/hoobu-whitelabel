<!--breadcrumb-section starts-->
<div class="breadcrumb-section">
    <div class="container">
        <h1>{$category.cat_name}</h1>
    </div>
</div>
<div class="container">
    <div class="breadcrumb">
        <a href="{$STORE_URL}">Home</a>
        <span class="fa fa-angle-double-right"></span>
        <span class="current">{$category.cat_name}</span>
    </div>
</div>
<!--breadcrumb-section ends-->

<div class="container">
	<section id="primary" class="with-sidebar primary-inner">
    	{if isset($SUBCATS) && $SUBCATS}
        <ul class="products">
        	{foreach from=$SUBCATS item=subcat}
            <li class="dt-sc-one-fourth column">
                <div class="dt-sc-ico-content type1">
                    <a href="{$subcat.url}" title="{$subcat.cat_name}">
                        <img class="th" src="{$subcat.cat_image}" alt="{$subcat.cat_name}">
                    </a>
                    <h4><a href="{$subcat.url}" title="{$subcat.cat_name}">{$subcat.cat_name}</a></h4>
                </div>
            </li>
            {/foreach}
        </ul>
        {/if}
    
    	{if $PRODUCTS}
        <ul class="products">
        	{foreach from=$PRODUCTS item=product}
            <li class="dt-sc-one-fourth column">
            	<form action="{$VAL_SELF}" method="post" class="panel add_to_basket">
                	<input type="hidden" name="add" value="{$product.product_id}">
                    <div class="product-thumb">
                        <a href="#">
                            <img src="{$product.thumbnail}" alt="{$product.name}">
                        </a>  
                        <div class="image-overlay">  
                            <div class="product-button">
                                <button type="submit" value="{$LANG.catalogue.add_to_basket}" class="add-cart-btn">{$LANG.catalogue.add_to_basket}</button>
                            </div>
                        </div>
                    </div>
                    <div class="product-details">
                        <h5><a href="{$product.url}" title="{$product.name}">{$product.name}</a></h5>
                        <p>
						{if $product.ctrl_sale}
                        <span class="sale-price">{$product.sale_price}</span><span class="price-sale">{$product.price}</span>
                        {else}
                        <span class="price">{$product.price}</span>
                        {/if}
                        </p>
                    </div>
                </form>
            </li>
            {/foreach}
        </ul>
        {/if}
	</section>
    
    <!--secondary starts-->
    <section id="secondary">
    
        <aside class="widget widget_categories">
            <h3 class="widgettitle">Categories</h3>
            <ul>
                <li>
                    <a href="">Play School<span>(16)</span></a>
                </li>
                <li>
                    <a href="">Academic Performance<span>(3)</span></a>
                </li>
                <li>
                    <a href="">Co-curricular<span>(26)</span></a>
                </li>
                <li>
                    <a href="">Visual Education<span>(18)</span></a>
                </li>
                <li>
                    <a href="">Inter Competition<span>(4)</span></a>
                </li>
            </ul>
        </aside>
        
        {if $PRODUCTS}
        <aside class="widget widget_text">
            <h3 class="widgettitle">{$category.cat_name}</h3>
            {if !empty($category.cat_desc)}
            	{$category.cat_desc}
            {/if}
        </aside>
        {/if}
        
        <aside class="widget widget_text">
            <h3 class="widgettitle">Visual Guidance</h3>
            <p>Our methods of teaching and level of quality instructors all add up to a well-rounded experience.</p>
            <iframe src="http://player.vimeo.com/video/21195297" width="420" height="200"></iframe>
        </aside>
        
        <aside class="widget widget_recent_entries">
            <h3 class="widgettitle">Kids Voices</h3>
            <!--dt-sc-tabs-container starts-->            
            <div class="dt-sc-tabs-container">
                <ul class="dt-sc-tabs">
                    <li><a href="#"> New </a></li> 
                    <li><a href="#"> Popular </a></li>
                </ul>
                <div class="dt-sc-tabs-content">
                    <h5><a href="">Explore your Thoughts!</a></h5>
                    <p>Nam consectetur justo non nis dapibus, ac commodo mi sagittis. Integer enim odio.</p>
                    <h5><a href="">Perform for Success!</a></h5>
                    <p>Sed ut perspiciatis unde omi iste natus error siterrecte voluptatem accusantium doloremque laudantium.</p>
                </div>
                <div class="dt-sc-tabs-content">
                    <h5><a href="">Admire &amp; Achieve!</a></h5>
                    <p>Sed ut perspiciatis unde omi iste natus error siterrecte voluptatem accusantium doloremque laudantium.</p>
                    <h5><a href="">Your Opportuntiy!</a></h5>
                    <p>Nam consectetur justo non nis dapibus, ac commodo mi sagittis. Integer enim odio.</p>
                </div>
            </div>
            <!--dt-sc-tabs-container ends-->
        </aside>
        
    </section>
    <!--secondary ends-->
</div>