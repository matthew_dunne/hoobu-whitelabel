<!--slider starts-->
<div id="slider">
	<div id="layerslider_4" class="ls-wp-container" style="width:100%;height:610px;max-width:1920px;margin:0 auto;margin-bottom: 0px;">   
		<div class="ls-slide">
			<img src="http://placehold.it/1920x610/000000" class="ls-bg" alt="Slide background" />   
		</div>
		<div class="ls-slide">
			<img src="http://placehold.it/1920x610/000000" class="ls-bg" alt="Slide background" />   
		</div>
		<div class="ls-slide">
			<img src="http://placehold.it/1920x610/000000" class="ls-bg" alt="Slide background" />   
		</div>
	</div>
</div>
<!--slider ends-->

<!-- categories starts -->
<section class="fullwidth-background categories-bg">
	<!--container starts-->
	<div class="container">
		<div class="dt-sc-one-fourth column">
			<div class="dt-sc-ico-content type1">
				<img src="http://placehold.it/275x275" alt="" title="">
				<h4><a href="">Active Learning</a></h4>
				<p>Curabitur ultrices posuere mattis. Nam ullamcorper, diam sit amet euismod pelleo ntesque, eros risus rhoncus libero, invest tibulum nisl ligula</p>
			</div>
		</div>
		
		<div class="dt-sc-one-fourth column">
			<div class="dt-sc-ico-content type1">
				<img src="http://placehold.it/275x275" alt="" title="">
				<h4><a href="">Music Class</a></h4>
				<p>Decor ostdcaer urabitur ultrices posuere mattis. Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, invest tibulum nisl ligula</p>
			</div>
		</div>
		
		<div class="dt-sc-one-fourth column">
			<div class="dt-sc-ico-content type1">
				 <img src="http://placehold.it/275x275" alt="" title="">
				<h4><a href="">Yoga Class</a></h4>
				<p>Rabitur ultrices posuere mattis. Nam ullamcorper, diam sit  euismod pelleo ntesque, eros risus rhoncus libero, invest tibulum nisl gedretu osterftra ligula</p>
			</div>
		</div>
		
		<div class="dt-sc-one-fourth column">
			<div class="dt-sc-ico-content type1">
				<img src="http://placehold.it/275x275" alt="" title="">
				<h4><a href="">Kung fu Class</a></h4>
				<p>Curabitur ultrices posuere mattis. Nam ullamcorper, diam sit amet euismod pelleo ntesque, eros risus rhoncus libero, invest tibulum nisl ligula</p>
			</div>
		</div>
	</div>
	<!--container ends-->
</section>
<!-- categories ends -->

	
<!-- info section starts -->
<section class="fullwidth-background info-bg">
	<!--container starts-->
	<div class="container">
		<!--dt-sc-one-half starts-->
		<div class="dt-sc-one-half column first">
			<h2>Play As You Learn</h2>
			<!--dt-sc-one-half starts-->
			<div class="dt-sc-one-half column first">
				
				<div class="dt-sc-ico-content type2">
					<div class="icon"> 
						<span class="fa fa-glass"> </span> 
					</div>
					<h4><a href="#" target="_blank"> English Summer Camp </a></h4>
					<p>Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, inst tibulum nisl ligula....</p>
				</div>
				<div class="dt-sc-hr-very-small"></div>
				<div class="dt-sc-ico-content type2">
					<div class="icon"> 
						<span class="fa fa-pencil"> </span> 
					</div>
					<h4><a href="#" target="_blank"> Drawing & Painting </a></h4>
					<p>Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, inst tibulum nisl ligula....</p>
				</div>
				<div class="dt-sc-hr-very-small"></div>
				<div class="dt-sc-ico-content type2">
					<div class="icon"> 
						<span class="fa fa-bullseye"> </span> 
					</div>
					<h4><a href="#" target="_blank"> Swimming Camp </a></h4>
					<p>Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, inst tibulum nisl ligula....</p>
				</div>
			
			</div>
			<!--dt-sc-one-half ends-->
			
			<!--dt-sc-one-half starts-->
			<div class="dt-sc-one-half column">
				
				<div class="dt-sc-ico-content type2">
					<div class="icon"> 
						<span class="fa fa-tachometer"> </span> 
					</div>
					<h4><a href="#" target="_blank"> Sports Camp </a></h4>
					<p>Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, inst tibulum nisl ligula....</p>
				</div>
				<div class="dt-sc-hr-very-small"></div>
				<div class="dt-sc-ico-content type2">
					<div class="icon"> 
						<span class="fa fa-magic"> </span> 
					</div>
					<h4><a href="#" target="_blank"> Personalizing </a></h4>
					<p>Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, inst tibulum nisl ligula....</p>
				</div>
				<div class="dt-sc-hr-very-small"></div>
				<div class="dt-sc-ico-content type2">
					<div class="icon"> 
						<span class="fa fa-music"> </span> 
					</div>
					<h4><a href="#" target="_blank"> Sing & Dance </a></h4>
					<p>Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, inst tibulum nisl ligula....</p>
				</div>
			
			</div>
			<!--dt-sc-one-half ends-->
		</div>
		<!--dt-sc-one-half ends-->
		
		<!--dt-sc-one-half starts-->
		<div class="dt-sc-one-half column">
			<h2>With Music4Kids, music is child's play!</h2>
			<div class="add-slider-wrapper">
				<ul class="add-slider">
					<li> <img src="http://placehold.it/464x345" alt="" title=""> </li>
					<li> <img src="http://placehold.it/464x345" alt="" title=""> </li>
					<li> <img src="http://placehold.it/464x345" alt="" title=""> </li>
				</ul>
			</div>
		</div>
		<!--dt-sc-one-half ends-->
	</div>	
	<!--container ends-->
</section>
<!-- info section starts -->
	
<!-- recent blog starts -->
<section class="fullwidth-background recent-blog-bg">
	<!--container starts-->
	<div class="container">
		<h2 class="dt-sc-hr-black-title">Recent Blog</h2>
		<div class="column dt-sc-one-half first">
			<article class="blog-entry">
				<div class="blog-entry-inner">		
					<div class="entry-details">	
						<div class="entry-title">
							<h3><a href="blog-detail.html"> Activities Improves Mind </a></h3>
							<p>27 August 2015</p>
						</div>			
						<!--entry-metadata ends-->	
						<div class="entry-body">
							<p>Roin bibendum nibhsds. Nuncsdsd fermdada msit ametadasd consequat. Praes porr nulla sit amet dui lobortis, id venenatis nibh accums.</p>
						</div>	 		
						<a href="blog-detail.html" class="dt-sc-button small"> Read More <span class="fa fa-chevron-circle-right"> </span></a>		
					</div>	
				</div>
			</article>
		</div>
		<!--dt-sc-one-half ends-->
		<div class="column dt-sc-one-half first">
			<article class="blog-entry">
				<div class="blog-entry-inner">		
					<div class="entry-details">	
						<div class="entry-title">
							<h3><a href="blog-detail.html"> Activities Improves Mind </a></h3>
							<p>27 August 2015</p>
						</div>			
						<!--entry-metadata ends-->	
						<div class="entry-body">
							<p>Roin bibendum nibhsds. Nuncsdsd fermdada msit ametadasd consequat. Praes porr nulla sit amet dui lobortis, id venenatis nibh accums.</p>
						</div>	 		
						<a href="blog-detail.html" class="dt-sc-button small"> Read More <span class="fa fa-chevron-circle-right"> </span></a>		
					</div>	
				</div>
			</article>
		</div>
		<!--dt-sc-one-half ends-->
	</div>
	<!--container ends-->    
</section>
<!-- recent blog starts -->


<!-- recent products starts -->
<section class="fullwidth-background recent-products-bg">
	<!--container starts-->
    <div class="container">
        <h2 class="dt-sc-hr-black-title">{$LANG.catalogue.latest_products}</h2>
        <ul class="products">
        	{foreach from=$LATEST_PRODUCTS item=product}
            <li class="dt-sc-one-fourth column first">
            	<form action="{$VAL_SELF}" method="post" class="panel add_to_basket">
                    <div class="product-thumb">
                        <a class="th" href="{$product.url}" title="{$product.name}"><img src="{$product.image}" alt="{$product.name}"></a> 
                        <div class="image-overlay">  
                            <div class="product-button">
                            	<button type="submit" value="{$LANG.catalogue.add_to_basket}" class="add-cart-btn">{$LANG.catalogue.add_to_basket}</button>
                            </div>
                        </div>
                    </div>
                    <div class="product-details">
                        <h5><a href="{$product.url}" title="{$product.name}">{$product.name|truncate:38:"&hellip;"}</a></h5>
                        <p>
						{if $product.ctrl_sale}
                        <span class="sale-price">{$product.sale_price}</span><span class="price-sale">{$product.price}</span>
                        {else}
                        <span class="price">{$product.price}</span>
                        {/if}
                        </p>
                    </div>
                	<input type="hidden" name="add" value="{$product.product_id}">
         		</form>
            </li>
            {/foreach}
        </ul>
    </div>
	<!--container ends-->
</section>
<!-- recent products ends -->
 
 
 
 