<!DOCTYPE html>
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml" dir="{$TEXT_DIRECTION}" lang="{$HTML_LANG}">
<head>
<title>{$META_TITLE}</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="{$CANONICAL}" rel="canonical">
<link href="{$STORE_URL}/favicon.ico" rel="shortcut icon" type="image/x-icon">
<meta http-equiv="content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Kids Life- Home</title>
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="{$STORE_URL}/skins/{$SKIN_FOLDER}/css/style.css" rel="stylesheet" type="text/css">
<link href="{$STORE_URL}/skins/{$SKIN_FOLDER}/css/shortcodes.css" rel="stylesheet" type="text/css">
<link href="{$STORE_URL}/skins/{$SKIN_FOLDER}/css/responsive.css" rel="stylesheet" type="text/css">
<link href="{$STORE_URL}/skins/{$SKIN_FOLDER}/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="{$STORE_URL}/skins/{$SKIN_FOLDER}/css/shortcodes.css" rel="stylesheet" type="text/css">
<link href="{$STORE_URL}/skins/{$SKIN_FOLDER}/css/hoobu-custom.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{$STORE_URL}/skins/{$SKIN_FOLDER}/css/layerslider.css" type="text/css">
<link rel="stylesheet" type="text/css" href="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/slick/slick-theme.css"/>
<!--prettyPhoto-->
<link href="{$STORE_URL}/skins/{$SKIN_FOLDER}/css/prettyPhoto.css" rel="stylesheet" type="text/css" media="all" />
<!--[if IE 7]>
        <link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
        <![endif]-->
<!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<!--Fonts-->
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/modernizr-2.6.2.min.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset={$CHARACTER_SET}">
<meta name="description" content="{if isset($META_DESCRIPTION)}{$META_DESCRIPTION}{/if}">
<meta name="keywords" content="{if isset($META_KEYWORDS)}{$META_KEYWORDS}{/if}">
<meta name="robots" content="index, follow">
<meta name="generator" content="cubecart">
{if $FBOG}
<meta property="og:image" content="{$PRODUCT.thumbnail}">
<meta property="og:url" content="{$VAL_SELF}">
{/if}
        {include file='templates/content.recaptcha.head.php'}
        {include file='templates/element.google_analytics.php'}
        {foreach from=$HEAD_JS item=js}{$js}{/foreach}
</head>

<body class="main">
{if $STORE_OFFLINE}
<div data-alert class="alert-box alert">{$LANG.common.warning_offline}<a href="#" class="close">&times;</a></div>
{/if} 

<!--wrapper starts-->
<div class="wrapper"> 
  <!--top menu starts-->
  <div id="top-menu">
    <div class="container">
      <div class="dt-sc-one-half column first">
        <p class="contact editable" contenteditable="true" id="contact_phone_email"> <i>+1 959 552 5963</i> <span class="fa fa-phone"></span> &nbsp;&nbsp;&nbsp; <a href="#">contact@kidslife.com</a> <span class="fa fa-envelope"></span> </p>
      </div>
      <div class="dt-sc-one-half column">
        <div class="cart-login">
          <p>{include file='templates/box.session.php'}
            {if $smarty.get._a != 'basket' && $smarty.get._a != 'confirm' && $smarty.get._a != 'gateway'}{include file='templates/box.basket.php'}{/if}</p>
        </div>
      </div>
    </div>
  </div>
  <!--top menu end--> 
  
  <!--header starts-->
  <header>
    <div class="container">
      <div class="logo"> <a href="{$STORE_URL}" class="main-logo"><img src="{$STORE_LOGO}" alt="{$META_TITLE}"></a> </div>
      	{include file='templates/box.search.php'} 
      </div>
    <!--menu-container starts-->
    <div id="menu-container">
      <div class="container"> 
        {include file='templates/box.navigation.php'} 
        
        <ul class="dt-sc-social-icons">
          <li><a href="#" title="Facebook" class="dt-sc-tooltip-top facebook"><span class="fa fa-facebook"></span></a></li>
          <li><a href="#" title="Youtube" class="dt-sc-tooltip-top youtube"><span class="fa fa-youtube"></span></a></li>
          <li><a href="#" title="Twitter" class="dt-sc-tooltip-top twitter"><span class="fa fa-twitter"></span></a></li>
          <li><a href="#" title="Google Plus" class="dt-sc-tooltip-top gplus"><span class="fa fa-google-plus"></span></a></li>
        </ul>
      </div>
    </div>
    <!--menu-container ends--> 
  </header>
  <!--header ends--> 
  
  <!--main starts-->
  <div id="main">
  	{$PAGE_CONTENT}
  </div>
  <!--main ends--> 
  
  <!--footer starts-->
  <footer> 
    <!--footer-widgets-wrapper starts-->
    <div class="footer-widgets-wrapper"> 
      <!--container starts-->
      <div class="container">
      	<div class="column dt-sc-one-fourth first">
          <aside class="widget tweetbox">
            {$SITE_DOCS}
          </aside>
        </div>
        <div class="column dt-sc-one-fourth">
          <aside class="widget widget_text">
            <h3 class="widgettitle red_sketch"> About Kids Life </h3>
            <p>Happy <a href=""><strong>Kids Life</strong></a> comes with powerful theme options, which empowers you to quickly and easily build incredible store.</p>
            <ul>
              <li> <a href=""> English Grammar Class </a> </li>
              <li> <a href=""> Music class </a> </li>
              <li> <a href=""> Swimming & Karate </a> </li>
              <li> <a href=""> Lot of HTML Styles </a> </li>
              <li> <a href=""> Unique News Page Design </a> </li>
            </ul>
          </aside>
        </div>
        <div class="column dt-sc-one-fourth">
          <aside class="widget widget_recent_entries">
            <h3 class="widgettitle green_sketch"> Latest Post </h3>
            <ul>
              <li> <a href=""> <img src="http://placehold.it/60x60" alt="" title=""> </a>
                <h6><a href=""> Amazing post with all  goodies </a></h6>
                <span> March 23, 2014 </span> </li>
              <li> <a href=""> <img src="http://placehold.it/60x60" alt="" title=""> </a>
                <h6><a href=""> Amazing post with all  goodies </a></h6>
                <span> March 23, 2014 </span> </li>
              <li> <a href=""> <img src="http://placehold.it/60x60" alt="" title=""> </a>
                <h6><a href=""> Amazing post with all  goodies </a></h6>
                <span> March 23, 2014 </span> </li>
            </ul>
          </aside>
        </div>
        <div class="column dt-sc-one-fourth">
          <aside class="widget widget_text">
            <h3 class="widgettitle steelblue_sketch">Contact</h3>
            <div class="textwidget">
              <p class="dt-sc-contact-info"><span class="fa fa-map-marker"></span>InkJet2U, 410 E. Russell St<br/>Fayetteville, NC 28301</p>
              <p class="dt-sc-contact-info"><span class="fa fa-phone"></span>xxx</p>
              <p class="dt-sc-contact-info"><span class="fa fa-envelope"></span><a href="mailto:yourname@somemail.com">xxx</a></p>
            </div>
          </aside>
          <aside class="widget newsletter-subscribe">
            <p> We're social </p>
            <form name="frmnewsletter" class="newsletter-subscribe-form" action="php/subscribe.php" method="post">
              <p> <span class="fa fa-envelope-o"> </span>
                <input type="email" placeholder="Email Address" name="mc_email" required />
              </p>
              <input type="submit" value="Subscribe" class="button" name="btnsubscribe">
            </form>
            <div id="ajax_subscribe_msg"></div>
          </aside>
        </div>
      </div>
      <!--container ends--> 
    </div>
    <!--footer-widgets-wrapper ends-->
    <div class="copyright">
      <div class="container">
        <p class="copyright-info">© 2015 InkJet2U. All rights reserved. Powered by <a href="http://www.hoobu.co.uk" title="">Hoobu</a></p>
        <div class="footer-links">
          <p>Follow us</p>
          <ul class="dt-sc-social-icons">
            <li class="facebook"><a href="#"><img src="{$STORE_URL}/skins/{$SKIN_FOLDER}/images/site/facebook.png" alt="" title=""></a></li>
            <li class="twitter"><a href="#"><img src="{$STORE_URL}/skins/{$SKIN_FOLDER}/images/site//twitter.png" alt="" title=""></a></li>
            <li class="gplus"><a href="#"><img src="{$STORE_URL}/skins/{$SKIN_FOLDER}/images/site//gplus.png" alt="" title=""></a></li>
            <li class="pinterest"><a href="#"><img src="{$STORE_URL}/skins/{$SKIN_FOLDER}/images/site//pinterest.png" alt="" title=""></a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  <!--footer ends--> 
  
</div>
<!--wrapper ends--> 
<a href="" title="Go to Top" class="back-to-top"><img src="{$STORE_URL}/skins/{$SKIN_FOLDER}/images/site/to-top.png" /></a> 
<!--Java Scripts--> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/jquery.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/jquery-migrate.min.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/jquery.validate.min.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/jquery-easing-1.3.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/jquery.sticky.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/jquery.nicescroll.min.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/jquery.inview.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/validation.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/jquery.tipTip.minified.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/jquery.bxslider.min.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/jquery.prettyPhoto.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/twitter/jquery.tweet.min.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/jquery.parallax-1.1.3.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/shortcodes.js"></script> 

<!-- Layer Slider --> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/jquery-transit-modified.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/layerslider.kreaturamedia.jquery.js"></script> 
<script type='text/javascript' src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/greensock.js"></script> 
<script type='text/javascript' src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/layerslider.transitions.js"></script> 

<!-- For Google Map -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/fancybox/source/jquery.fancybox.js?v=2.1.5"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/slick/slick.min.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/custom.js"></script> 
<script type="text/javascript" src="{$STORE_URL}/skins/{$SKIN_FOLDER}/js/script.js"></script>

<!--<script type="text/javascript">var lsjQuery = jQuery;</script>--> 
{literal} 
<script type="text/javascript">
var lsjQuery = jQuery;
lsjQuery(document).ready(function() {
	if(typeof lsjQuery.fn.layerSlider == "undefined") { 
		lsShowNotice('layerslider_1','jquery');
	} else { 
		lsjQuery("#layerslider_4").layerSlider({responsiveUnder: 1240, layersContainer: 1060, skinsPath: 'skins/hoobuwl1/js/layerslider/skins/'})
	}
}); 
</script> 
{/literal}

{$DEBUG_INFO}
</body>
</html>