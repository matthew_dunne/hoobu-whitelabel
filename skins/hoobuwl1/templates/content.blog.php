<!--breadcrumb-section starts-->
<div class="breadcrumb-section">
    <div class="container">
        <h1>Blog</h1>
    </div>
</div>
<div class="container">
    <div class="breadcrumb">
        <a href="{$STORE_URL}">Home</a>
        <span class="fa fa-angle-double-right"></span>
        <span class="current">Blog</span>
    </div>
</div>
<!--breadcrumb-section ends-->

<section id="primary" class="content-full-width">
  <div class="container">
        <div class="dt-sc-one-column">
            <article class="blog-entry">
                <div class="blog-entry-inner">		
                    <div class="entry-details">	
                        <div class="entry-title">
                            <h3><a href="blog-detail.html"> Activities Improves Mind </a></h3>
                            <p>27 August 2015</p>
                        </div>			
                        <!--entry-metadata ends-->	
                        <div class="entry-body">
                            <p>Roin bibendum nibhsds. Nuncsdsd fermdada msit ametadasd consequat. Praes porr nulla sit amet dui lobortis, id venenatis nibh accums. Roin bibendum nibhsds. Nuncsdsd fermdada msit ametadasd consequat. Praes porr nulla sit amet dui lobortis, id venenatis nibh accums.<</p>
                        </div>	 		
                        <a href="blog-detail.html" class="dt-sc-button small"> Read More <span class="fa fa-chevron-circle-right"> </span></a>		
                    </div>	
                </div>
            </article>
        </div>
        
        <div class="dt-sc-one-column">
            <article class="blog-entry">
                <div class="blog-entry-inner">		
                    <div class="entry-details">	
                        <div class="entry-title">
                            <h3><a href="blog-detail.html"> Activities Improves Mind </a></h3>
                            <p>27 August 2015</p>
                        </div>			
                        <!--entry-metadata ends-->	
                        <div class="entry-body">
                            <p>Roin bibendum nibhsds. Nuncsdsd fermdada msit ametadasd consequat. Praes porr nulla sit amet dui lobortis, id venenatis nibh accums.</p>
                        </div>	 		
                        <a href="blog-detail.html" class="dt-sc-button small"> Read More <span class="fa fa-chevron-circle-right"> </span></a>		
                    </div>	
                </div>
            </article>
        </div>
        
        <div class="dt-sc-one-column">
            <article class="blog-entry">
                <div class="blog-entry-inner">		
                    <div class="entry-details">	
                        <div class="entry-title">
                            <h3><a href="blog-detail.html"> Activities Improves Mind </a></h3>
                            <p>27 August 2015</p>
                        </div>			
                        <!--entry-metadata ends-->	
                        <div class="entry-body">
                            <p>Roin bibendum nibhsds. Nuncsdsd fermdada msit ametadasd consequat. Praes porr nulla sit amet dui lobortis, id venenatis nibh accums.</p>
                        </div>	 		
                        <a href="blog-detail.html" class="dt-sc-button small"> Read More <span class="fa fa-chevron-circle-right"> </span></a>		
                    </div>	
                </div>
            </article>
        </div>
  </div>
</section>