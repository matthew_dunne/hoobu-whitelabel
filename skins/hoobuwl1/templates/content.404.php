<section class="fullwidth-background">
	<div class="container">
        <h2>{$LANG.documents.404_title}</h2>
        <p>{$LANG.documents.404_content}</p>
    </div>
</section>