{if $CATEGORIES}
{$CATEGORIES}
{else}
<!--nav starts-->
<nav id="main-menu">
  <div class="dt-menu-toggle" id="dt-menu-toggle">Menu<span class="dt-menu-toggle-icon"></span></div>
  <ul id="menu-main-menu" class="menu">
	{$NAVIGATION_TREE}
    {if $CTRL_CERTIFICATES && !$CATALOGUE_MODE}
    <li><a href="{$STORE_URL}/gift-certificates.html" title="{$LANG.navigation.giftcerts}">{$LANG.navigation.giftcerts}</a></li>
    {/if}
    {if $CTRL_SALE}
    <li><a href="{$STORE_URL}/sale-items.html" title="{$LANG.navigation.saleitems}">{$LANG.navigation.saleitems}</a></li>
    {/if}
  </ul>
</nav>
<!--nav ends-->{/if}





