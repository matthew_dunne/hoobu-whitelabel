{if isset($ITEMS)}
<div id="cart">
    <table>
      <thead>
        <tr>
          <th class="remove">Remove</th>
          <th class="image">Image</th>
          <th class="item">{$LANG.common.name}</th>
          <th class="unit_price">{$LANG.common.price_unit}</th>
          <th class="qty">{$LANG.common.quantity}</th>
          <th class="price">{$LANG.common.price}</th>
        </tr>
      </thead>
      <tbody>
        {foreach from=$ITEMS key=hash item=item}
        <tr>
          <td class="remove"><a href="{$STORE_URL}/index.php?_a=basket&remove-item={$hash}" class="cart"><i class="fa fa-times"></i></a></td> 
          <td class="image">
            <div class="product_image">
              <a href="{$item.link}" class="th" title="{$item.name}"><img src="{$item.image}" alt="{$item.name}" width="100"></a>
            </div>
          </td>
          <td class="item cell-align-left">
            <a href="{$item.link}"><strong>{$item.name}</strong></a>
          </td>
          <td class="unit_price">
            {$item.line_price_display}
          </td>
          <td class="qty">
            <input name="quan[{$hash}]" type="text" value="{$item.quantity}" maxlength="3" class="quantity" {$QUAN_READ_ONLY}>
          </td>
          <td class="price"><span class=money>{$item.price_display}</span></td>
        </tr>
        {/foreach}
        
      </tbody>
      <tfoot>
         <tr>
            <td colspan="4">&nbsp;
            	
            </td>
            <td>{$LANG.basket.total_sub}</td>
            <td class="text-right">{$SUBTOTAL}</td>
         </tr>
         {if isset($SHIPPING)}
         <tr>
            <td colspan="4" class="shipping-method">
               {$LANG.basket.shipping_select}:
               <select name="shipping">
                  <option value="">{$LANG.form.please_select}</option>
                  {foreach from=$SHIPPING key=group item=methods}
                  {if $HIDE_OPTION_GROUPS ne '1'}
                  <optgroup label="{$group}">{/if}
                     {foreach from=$methods item=method}
                     <option value="{$method.value}" {$method.selected}>{$method.display}</option>
                     {/foreach}
                     {if $HIDE_OPTION_GROUPS ne '1'}
                  </optgroup>
                  {/if}
                  {/foreach}
               </select>
            </td>
            <td>{$LANG.basket.shipping}
               {if $ESTIMATE_SHIPPING}
               (<a href="#" onclick="$('#getEstimate').slideToggle();">{$LANG.common.estimated}</a>)
               <div id="getEstimate" class="hide panel callout">
                  <h4>{$LANG.basket.specify_shipping}</h4>
                  <label for="estimate_country">{$LANG.address.country}</label>
                  <select name="estimate[country]" id="estimate_country"  class="nosubmit country-list" rel="estimate_state">
                     {foreach from=$COUNTRIES item=country}<option value="{$country.numcode}" {$country.selected}>{$country.name}</option>{/foreach}
                  </select>
                  <label for="estimate_state">{$LANG.address.state}</label>
                  <input type="text" name="estimate[state]" id="estimate_state" value="{$ESTIMATES.state}" placeholder="{$LANG.address.state}">
                  <label for="estimate_postcode">{$LANG.address.postcode}</label>
                  <input type="text" value="{$ESTIMATES.postcode}" id="estimate_postcode" placeholder="{$LANG.address.postcode}" name="estimate[postcode]">
                  <input type="submit" name="get-estimate" class="button expand" value="{$LANG.basket.fetch_shipping_rates}">
                  <script type="text/javascript">
                  var county_list = {$STATE_JSON};
                  </script>
               </div>
               {/if}
            </td>
            <td class="text-right">{$SHIPPING_VALUE}</td>
         </tr>
         {/if}
         {foreach from=$TAXES item=tax}
         <tr>
            <td colspan="4"></td>
            <td>{$tax.name}{$CUSTOMER_LOCALE.mark}</td>
            <td class="text-right">{$tax.value}</td>
         </tr>
         {/foreach}
         {foreach from=$COUPONS item=coupon}
         <tr>
            <td colspan="4"></td>
            <td><a href="{$VAL_SELF}&remove_code={$coupon.remove_code}" title="{$LANG.common.remove}">{$coupon.voucher}</a></td>
            <td class="text-right">{$coupon.value}</td>
         </tr>
         {/foreach}
         {if isset($DISCOUNT)}
         <tr>
            <td colspan="4"></td>
            <td>{$LANG.basket.total_discount}</td>
            <td class="text-right">{$DISCOUNT}</td>
         </tr>
         {/if}
         <tr>
            <td colspan="4"></td>
            <td>{$LANG.basket.total_grand}</td>
            <td class="text-right">{$TOTAL}</td>
         </tr>
      </tfoot>
    </table>
        <!--
        <div class="span6 cart-buttons inner-right inner-left">
          <div class="buttons clearfix">
            <input type="submit" id="proceed" class="btn dt-sc-button medium" name="proceed" value="Check out" />
            <input type="submit" id="update-cart" class="btn dt-sc-button medium secondary" name="update" value="Update" />
          </div>
          
        </div>
        -->
</div>   
{/if}


