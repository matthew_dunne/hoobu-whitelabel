<!--breadcrumb-section starts-->
<div class="breadcrumb-section">
    <div class="container">
        <h1>{$LANG.account.your_account}</h1>
    </div>
</div>
<div class="container">
    <div class="breadcrumb">
        <a href="{$STORE_URL}">Home</a>
        <span class="fa fa-angle-double-right"></span>
        <span class="current">{$LANG.account.your_account}</span>
    </div>
</div>
<!--breadcrumb-section ends-->

<!-- categories starts -->
<section class="fullwidth-background">
	<!--container starts-->
	<div class="container">
		<div class="dt-sc-one-third column">
			<div class="dt-sc-ico-content type1">
                <a href="{$STORE_URL}/index.php?_a=profile" title="{$LANG.account.your_details}" class="button secondary expand nomarg"><i class="account-icon fa fa-user"></i></a>
				<h4 class="account-icon-name"><a href="{$STORE_URL}/index.php?_a=profile" title="{$LANG.account.your_details}" class="button secondary expand nomarg">{$LANG.account.your_details}</a></h4>
			</div>
		</div>
        <div class="dt-sc-one-third column">
			<div class="dt-sc-ico-content type1">
            	<a href="{$STORE_URL}/index.php?_a=addressbook" title="{$LANG.account.your_addressbook}" class="button secondary expand nomarg"><i class="account-icon fa fa-book"></i></a>
				<h4 class="account-icon-name"><a href="{$STORE_URL}/index.php?_a=addressbook" title="{$LANG.account.your_addressbook}" class="button secondary expand nomarg">{$LANG.account.your_addressbook}</a></h4>
			</div>
		</div>
        <div class="dt-sc-one-third column">
			<div class="dt-sc-ico-content type1">
            	<a href="{$STORE_URL}/index.php?_a=vieworder" title="{$LANG.account.your_orders}" class="button secondary expand nomarg"><i class="account-icon fa fa-truck"></i></a>
				<h4 class="account-icon-name"><a href="{$STORE_URL}/index.php?_a=vieworder" title="{$LANG.account.your_orders}" class="button secondary expand nomarg">{$LANG.account.your_orders}</a></h4>
			</div>
		</div>
    </div>
</section>
