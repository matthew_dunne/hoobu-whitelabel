{if isset($PRODUCT) && $PRODUCT}
<!--breadcrumb-section starts-->
<div class="breadcrumb-section">
    <div class="container">
        <h1>{$PRODUCT.name}</h1>
    </div>
</div>
<div class="container">
    <div class="breadcrumb">
        <a href="{$STORE_URL}">{$LANG.common.home}</a>
        {foreach from=$CRUMBS item=crumb}
        <span class="fa fa-angle-double-right"></span>
        <a href="{$crumb.url}" title="{$crumb.title}">{$crumb.title}</a>
        {/foreach}
    </div>
</div>
<!--breadcrumb-section ends-->

<!--container starts-->
<div class="container">
    <!--primary starts-->
    <section id="primary" class="with-sidebar">
    
        <div class="product-images">
            <div class="main">
                <a href="{$PRODUCT.medium}" class="fancybox" data-fancybox-group="gallery">
                    <img src="{$PRODUCT.medium}" alt="{$PRODUCT.name}" title="">
                </a> 
            </div>
            {if $GALLERY}
            <div class="thumbs">
            	{foreach from=$GALLERY item=image}
                <div class="image">
                    <a href="{$image.medium}" class="fancybox" data-fancybox-group="gallery" title="">
                        <img src="{$image.small}" alt="{$LANG.catalogue.click_enlarge}" />
                    </a>
                </div>
                {/foreach}
            </div>
            {/if}
        </div>
        <div class="summary">
            <h2>{$PRODUCT.name}</h2>
            <p class="price">
                {if $PRODUCT.ctrl_sale}<del>{$PRODUCT.price}</del>
                <span>{$PRODUCT.sale_price}</span>
                {else}
                <span>{$PRODUCT.price}</span>
                {/if}
            </p>
            
            {$PRODUCT.description_short}
            
            <form action="{$VAL_SELF}" method="post" class="cart">
                <input type="text" name="quantity" value="1">
                <input type="hidden" name="add" value="{$PRODUCT.product_id}">
                <button type="submit" value="{$LANG.catalogue.add_to_basket}" class="dt-sc-button medium">{$LANG.catalogue.add_to_basket}</button>
            </form>
        </div>
        
        <div class="dt-sc-hr"></div>
        
        <!--dt-sc-tabs-container starts-->            
        <div class="dt-sc-tabs-container">
            <ul class="dt-sc-tabs">
                <li><a href="#"> Description </a></li> 
                <li><a href="#"> Reviews (5) </a></li>
            </ul>
            <div class="dt-sc-tabs-content">
                <h2>Product Description</h2>
                {$PRODUCT.description}
            </div>
            <div class="dt-sc-tabs-content">
                <h2>4 Reviews for Pretty Little Girl</h2>
                <div id="comments">
                    <ol class="commentlist">
                        <li>
                            <div class="comment_container">
                                <img src="http://placehold.it/100x100" alt="" title="">
                            </div>
                            <div class="comment-text">
                                <div class="rating-review">
                                    <span class="author-rating rating-5"></span> <a href="#">5 reviews</a>
                                </div>
                                <p>Nam consectetur justo non nis dapibus, ac commodo mi sagittis. Integer enim odio. In lobortis rhoncus pulvinar. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                            </div>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <!--dt-sc-tabs-container ends-->
        
        <div class="related">
         
        <h2>Related Products</h2>
            <ul class="products">
               
                <li class="dt-sc-one-third column first">
                    <div class="product-thumb">
                        <a href="#">
                            <img src="http://placehold.it/510x510" alt="" title="">
                        </a>  
                        <div class="image-overlay">  
                            <div class="product-button">
                                <a href="#" class="add-cart-btn"> Add to cart </a>
                            </div>
                        </div>
                    </div>
                    <div class="product-details">
                        <h5><a href="#"> Ellents Style Grade </a></h5>
                        <span class="price"> $20.00 </span>
                    </div>
                </li>
                <li class="dt-sc-one-third column">
                    <div class="product-thumb">
                        <a href="#">
                            <img src="http://placehold.it/510x510" alt="" title="">
                        </a>  
                        <div class="image-overlay">  
                            <div class="product-button">
                                <a href="#" class="add-cart-btn"> Add to cart </a>
                            </div>
                        </div>
                    </div>
                    <div class="product-details">
                        <h5><a href="#"> Ellents Style Grade </a></h5>
                        <span class="price"> $20.00 </span>
                    </div>
                </li>
                <li class="dt-sc-one-third column">
                    <div class="product-thumb">
                        <a href="#">
                            <img src="http://placehold.it/510x510" alt="" title="">
                        </a>  
                        <div class="image-overlay">  
                            <div class="product-button">
                                <a href="#" class="add-cart-btn"> Add to cart </a>
                            </div>
                        </div>
                    </div>
                    <div class="product-details">
                        <h5><a href="#"> Ellents Style Grade </a></h5>
                        <span class="price"> $20.00 </span>
                    </div>
                </li>
            </ul>
        </div>
    </section>
    <!--primary ends-->
    
    <!--secondary starts-->
    <section id="secondary">
    
        <aside class="widget widget_categories">
            <h3 class="widgettitle">Categories</h3>
            <ul>
                <li>
                    <a href="">Play School<span>(16)</span></a>
                </li>
                <li>
                    <a href="">Academic Performance<span>(3)</span></a>
                </li>
                <li>
                    <a href="">Co-curricular<span>(26)</span></a>
                </li>
                <li>
                    <a href="">Visual Education<span>(18)</span></a>
                </li>
                <li>
                    <a href="">Inter Competition<span>(4)</span></a>
                </li>
            </ul>
        </aside>
        
        <aside class="widget widget_text">
            <h3 class="widgettitle">Kids Achievements</h3>
            <p>In lobortis rhoncus pulvinar. Pellentesque habitant morbi tristique <a href="#" class="highlighter">senectus</a> et netus et malesuada fames ac turpis egestas. </p>
            <p>Sed tempus ligula ac mi iaculis lobortis. Nam consectetur justo non nisi dapibus, ac commodo mi sagittis. Integer enim odio.</p>
        </aside>
        
        <aside class="widget widget_text">
            <h3 class="widgettitle">Visual Guidance</h3>
            <p>Our methods of teaching and level of quality instructors all add up to a well-rounded experience.</p>
            <iframe src="http://player.vimeo.com/video/21195297" width="420" height="200"></iframe>
        </aside>
        
        <aside class="widget widget_recent_entries">
            <h3 class="widgettitle">Kids Voices</h3>
            <!--dt-sc-tabs-container starts-->            
            <div class="dt-sc-tabs-container">
                <ul class="dt-sc-tabs">
                    <li><a href="#"> New </a></li> 
                    <li><a href="#"> Popular </a></li>
                </ul>
                <div class="dt-sc-tabs-content">
                    <h5><a href="">Explore your Thoughts!</a></h5>
                    <p>Nam consectetur justo non nis dapibus, ac commodo mi sagittis. Integer enim odio.</p>
                    <h5><a href="">Perform for Success!</a></h5>
                    <p>Sed ut perspiciatis unde omi iste natus error siterrecte voluptatem accusantium doloremque laudantium.</p>
                </div>
                <div class="dt-sc-tabs-content">
                    <h5><a href="">Admire &amp; Achieve!</a></h5>
                    <p>Sed ut perspiciatis unde omi iste natus error siterrecte voluptatem accusantium doloremque laudantium.</p>
                    <h5><a href="">Your Opportuntiy!</a></h5>
                    <p>Nam consectetur justo non nis dapibus, ac commodo mi sagittis. Integer enim odio.</p>
                </div>
            </div>
            <!--dt-sc-tabs-container ends-->
        </aside>
        
    </section>
    <!--secondary ends-->
    
</div>
<!--container ends-->
{else}
<p>{$LANG.catalogue.product_doesnt_exist}</p>
{/if}