{if isset($ITEMS)}
<form action="{$VAL_SELF}" method="post" enctype="multipart/form-data" class="autosubmit" id="checkout_form">
{if $INCLUDE_CHECKOUT}
{include file='templates/content.checkout.confirm.php'}
{/if}
<section id="primary" class="content-full-width">
  <div class="container">
    <div id="cart">
      <div class="row">
        <div class="span12">
          <form action="/cart" method="post" id="cartform">
            <table>
              <thead>
                <tr>
                  <th class="remove">Remove</th>
                  <th class="image">Image</th>
                  <th class="item">{$LANG.common.name}</th>
                  <th class="unit_price">{$LANG.common.price_unit}</th>
                  <th class="qty">{$LANG.common.quantity}</th>
                  <th class="price">{$LANG.common.price}</th>
                </tr>
              </thead>
              <tbody>
                {foreach from=$ITEMS key=hash item=item}
                <tr>
                  <td class="remove"><a href="{$STORE_URL}/index.php?_a=basket&remove-item={$hash}" class="cart"><i class="fa fa-times"></i></a></td> 
                  <td class="image">
                    <div class="product_image">
                      <a href="{$item.link}" class="th" title="{$item.name}"><img src="{$item.image}" alt="{$item.name}" width="100"></a>
                    </div>
                  </td>
                  <td class="item">
                    <a href="{$item.link}"><strong>{$item.name}</strong></a>
                  </td>
                  <td class="unit_price">
                    {$item.line_price_display}
                  </td>
                  <td class="qty">
                    <input name="quan[{$hash}]" type="text" value="{$item.quantity}" maxlength="3" class="quantity" {$QUAN_READ_ONLY}>
                  </td>
                  <td class="price"><span class=money>{$item.price_display}</span></td>
                </tr>
                {/foreach}
                
              </tbody>
            </table>
            <div class="Grand_Total">
              <p class="price">The Total of your cart is :  <span class="total"><strong><span class=money>{$item.price_display}</span></strong></span></p>
            </div>
            <div class="span6 cart-buttons inner-right inner-left">
              <div class="buttons clearfix">
                <input type="submit" id="proceed" class="btn dt-sc-button medium" name="proceed" value="Check out" />
                <input type="submit" id="update-cart" class="btn dt-sc-button medium secondary" name="update" value="Update" />
              </div>
              
            </div>
          </form>     
        </div>    
      </div>
    </div> 
</section>
{/if}


