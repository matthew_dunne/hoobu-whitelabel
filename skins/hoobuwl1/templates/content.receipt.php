<!--breadcrumb-section starts-->
<div class="breadcrumb-section">
    <div class="container">
        <h1>{$LANG.orders.order_number}: #{$SUM.cart_order_id}</h1>
    </div>
</div>
<div class="container">
    <div class="breadcrumb">
            <a href="{$STORE_URL}">Home</a>
        <span class="fa fa-angle-double-right"></span>
        <a href="{$STORE_URL}/index.php?_a=account">{$LANG.account.your_account}</a>
        <span class="fa fa-angle-double-right"></span>
        <a href="{$STORE_URL}/index.php?_a=vieworder">{$LANG.account.your_orders}</a>
        <span class="fa fa-angle-double-right"></span>
        <span class="current">#{$SUM.cart_order_id}</span>
    </div>
</div>
<!--breadcrumb-section ends-->

<section id="primary" class="content-full-width">
    <div class="container">
    	<div class="receipt">
            <div><strong>{$LANG.orders.title_order_status}:</strong> <span class="order_status_{$SUM.status}">{$SUM.order_status}</span></div>
            <div><strong>{$LANG.basket.order_date}:</strong> {$SUM.order_date_formatted}</div>
            <div class="customer-info">
                <h3>{$LANG.basket.customer_info}</h3>
               <div>
                  <strong>{$LANG.address.billing_address}</strong><br>
                  {$SUM.title} {$SUM.first_name} {$SUM.last_name}<br>
                  {if $SUM.company_name}{$SUM.company_name}<br>{/if}
                  {$SUM.line1}<br>
                  {if $SUM.line2}{$SUM.line2}<br>{/if}
                  {$SUM.town}<br>
                  {$SUM.state}, {$SUM.postcode}<br>
                  {$SUM.country}
               </div>
               <div class="delivery-address">
                  <strong>{$LANG.address.delivery_address}</strong><br>
                  {$SUM.title_d} {$SUM.first_name_d} {$SUM.last_name_d}<br>
                  {if $SUM.company_name_d}{$SUM.company_name_d}<br>{/if}
                  {$SUM.line1_d}<br>
                  {if $SUM.line2_d}{$SUM.line2_d}<br>{/if}
                  {$SUM.town_d}<br>
                  {$SUM.state_d}, {$SUM.postcode_d}<br>
                  {$SUM.country_d}
               </div>
            </div>
            {if $DELIVERY}
            <h4>{$LANG.common.delivery}</h4>
            {if !empty($DELIVERY.date) && $DELIVERY.date!=='0000-00-00'}
            <div class="row">
              <div class="small-6 medium-3 columns">{$LANG.orders.shipping_date}:</div>
              <div class="small-6 medium-9 columns">{$DELIVERY.date}</div>
            </div>
            {/if}
            {if !empty($DELIVERY.url)}
            <div class="row">
              <div class="small-6 medium-3 columns">{$LANG.orders.shipping_tracking}:</div>
              <div class="small-6 medium-9 columns"><a href="{$DELIVERY.url}" target="_blank">{$DELIVERY.method}{if !empty($DELIVERY.product)} ({$DELIVERY.product}){/if}</a></div>
            </div>
            {elseif !empty($DELIVERY.tracking)}
            <div class="row">
              <div class="small-6 medium-3 columns">{$LANG.catalogue.delivery_method}:</div>
              <div class="small-6 medium-9 columns">{$DELIVERY.method}{if !empty($DELIVERY.product)} ({$DELIVERY.product}){/if}</div>
            </div>
            <div class="row">
              <div class="small-6 medium-3 columns">{$LANG.orders.shipping_tracking}:</div>
              <div class="small-6 medium-9 columns">{$DELIVERY.tracking}</div>
            </div>
            {/if}
            {/if}
            <h3>{$LANG.basket.order_summary}</h3>
            <table class="expand">
               <thead>
                  <tr>
                     <th>{$LANG.common.product}</th>
                     <th>{$LANG.catalogue.price_each}</th>
                     <th>{$LANG.common.quantity}</th>
                     <th>{$LANG.common.price}</th>
                  </tr>
               </thead>
               <tbody>
                  {foreach from=$ITEMS item=item}
                  <tr>
                     <td class="cell-align-left">
                        {$item.name}{if !empty($item.product_code)} ({$item.product_code}){/if}
                        {if !empty($item.options)}
                        <p>{foreach from=$item.options item=option}{$option}<br>{/foreach}</p>
                        {/if}
                     </td>
                     <td class="text-right">{$item.price}</td>
                     <td class="text-center">{$item.quantity}</td>
                     <td class="text-right">{$item.price_total}</td>
                  </tr>
               </tbody>
               {/foreach}
               <tfoot>
                  <tr>
                     <td colspan="2"></td>
                     <td class="text-right">{$LANG.basket.total_sub}</td>
                     <td>{$SUM.subtotal}</td>
                  </tr>
                  <tr>
                     <td colspan="2"></td>
                     <td class="text-right">{if !empty($SUM.ship_method)}{$SUM.ship_method|replace:'_':' '}{if !empty($SUM.ship_product)} ({$SUM.ship_product}){/if}{else}{$LANG.basket.shipping}{/if}</td>
                     <td>{$SUM.shipping}</td>
                  </tr>
                  {foreach from=$TAXES item=tax}
                  <tr>
                     <td colspan="2"></td>
                     <td class="text-right">{$tax.name}</td>
                     <td>{$tax.value}</td>
                  </tr>
                  {/foreach}
                  {if $DISCOUNT}
                  <tr>
                     <td colspan="2"></td>
                     <td class="text-right">{$LANG.basket.total_discount}</td>
                     <td>{$SUM.discount}</td>
                  </tr>
                  {/if}
                  <tr>
                     <td colspan="2"></td>
                     <td class="text-right">{$LANG.basket.total_grand}</td>
                     <td>{$SUM.total}</td>
                  </tr>
               </tfoot>
            </table>
            {if !empty($SUM.note_to_customer)}
            <blockquote>{$SUM.note_to_customer}</blockquote>
            {/if}
            {if !empty($SUM.customer_comments)}
            <h3>{$LANG.common.comments}</h3>
            <p>&quot;{$SUM.customer_comments}&quot;</p>
            {/if}
            <p><a href="{$STORE_URL}/index.php?_a=receipt&cart_order_id={$SUM.cart_order_id}" target="_blank"><i class="fa fa-print"></i> {$LANG.confirm.print}</a></p>
            {foreach from=$AFFILIATES item=affiliate}{$affiliate}{/foreach}
            {if $ANALYTICS}
            <!-- Google Analytics for e-commerce -->
            <script type="text/javascript">
               {literal}
               var _gaq = _gaq || [];
               _gaq.push(['_setAccount', '{/literal}{$GA_SUM.google_id}{literal}']);
               _gaq.push(['_trackPageview']);
               _gaq.push(['_addTrans',
                 '{/literal}{$GA_SUM.cart_order_id}{literal}',           // order ID - required
                 '{/literal}{$GA_SUM.store_name}{literal}',  // affiliation or store name
                 '{/literal}{$GA_SUM.total}{literal}',          // total - required
                 '{/literal}{$GA_SUM.total_tax}{literal}',           // tax
                 '{/literal}{$GA_SUM.shipping}{literal}',              // shipping
                 '{/literal}{$GA_SUM.town}{literal}',       // city
                 '{/literal}{$GA_SUM.state}{literal}',     // state or province
                 '{/literal}{$GA_SUM.country_iso}{literal}'             // country
               ]);
               {/literal}
               
                // add item might be called for every item in the shopping cart
                // where your ecommerce engine loops through each item in the cart and
                // prints out _addItem for each
               {foreach from=$GA_ITEMS item=item}
               {literal}
               _gaq.push(['_addItem',
                 '{/literal}{$GA_SUM.cart_order_id}{literal}',           // order ID - required
                 '{/literal}{$item.product_code}{literal}',           // SKU/code - required
                 '{/literal}{$item.name}{literal}',        // product name
                 '',   // category or variation
                 '{/literal}{$item.price}{literal}',          // unit price - required
                 '{/literal}{$item.quantity}{literal}'               // quantity - required
               ]);
               {/literal}
               {/foreach}
               {literal}
               _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers
               
               (function() {
                 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
               })();
               {/literal}
            </script>
            {/if}
        </div>
  </div>
</section>