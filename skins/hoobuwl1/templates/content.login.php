<!--breadcrumb-section starts-->
<div class="breadcrumb-section">
    <div class="container">
        <h1>{$LANG.account.login}</h1>
    </div>
</div>
<div class="container">
    <div class="breadcrumb">
        <a href="{$STORE_URL}">Home</a>
        <span class="fa fa-angle-double-right"></span>
        <span class="current">{$LANG.account.login}</span>
    </div>
</div>
<!--breadcrumb-section ends-->
      
<section id="primary" class="content-full-width">
  <div class="container">
	<div id="customer-login" class="span7">		  
       <div class="row">
         <div id="login" class="span7">		  
         	<p class="show-for-small-only">{$LANG.account.want_to_signup} <a href="{$STORE_URL}/register.html">{$LANG.account.register_here}</a></p>
            <form action="{$VAL_SELF}" id="customer_login" method="post">
              <label for="customer_email" class="label">{$LANG.user.email_address}</label>
              <input type="text" name="username" id="login-username" placeholder="{$LANG.user.email_address} {$LANG.form.required}" value="{$USERNAME}" required>
          
              <label for="customer_password" class="label">{$LANG.account.password}</label>
              <input type="password" autocomplete="off" name="password" id="login-password" placeholder="{$LANG.account.password} {$LANG.form.required}" required>
          
              <p><a href="{$STORE_URL}/index.php?_a=recover">{$LANG.account.forgotten_password}</a></p>
          
              <input type="checkbox" name="remember" id="login-remember" value="1" {if $REMEMBER}checked{/if}><label for="login-remember">{$LANG.account.remember_me}</label>
          
              <p><button name="submit" type="submit" class="dt-sc-button medium"><i class="fa fa-sign-in"></i> {$LANG.account.log_in}</button></p>
          
              <input type="hidden" name="redir" value="{$REDIRECT_TO}">
            </form>
          </div>
        </div>
     </div>
     
	<div class="hide" id="validate_email">{$LANG.common.error_email_invalid}</div>
	<div class="hide" id="empty_password">{$LANG.account.error_password_empty}</div>
  
  </div>
</section>
