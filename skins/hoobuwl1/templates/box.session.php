{*
 * CubeCart v6
 * ========================================
 * CubeCart is a registered trade mark of CubeCart Limited
 * Copyright CubeCart Limited 2015. All rights reserved.
 * UK Private Limited Company No. 5323904
 * ========================================
 * Web:   http://www.cubecart.com
 * Email:  sales@cubecart.com
 * License:  GPL-3.0 https://www.gnu.org/licenses/quick-guide-gplv3.html
 *}
   {if $IS_USER}
   <a href="{$STORE_URL}/index.php?_a=account" title="{$LANG.account.your_account}">{$LANG.account.your_account}</a>&nbsp;&nbsp;|&nbsp;&nbsp;
   <a href="{$STORE_URL}/index.php?_a=logout" title="{$LANG.account.logout}">{$LANG.account.logout}</a>
   {else}
   <a href="{$STORE_URL}/login.html" id="customer_login_link">{$LANG.account.login}</a><span class="or">&nbsp;&nbsp;|&nbsp;&nbsp;</span><a href="{$STORE_URL}/register.html" id="customer_register_link">{$LANG.account.register}</a>
   {/if}
