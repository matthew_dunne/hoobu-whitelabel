<!--breadcrumb-section starts-->
<div class="breadcrumb-section">
    <div class="container">
        <h1>{$LANG.checkout.your_basket}</h1>
    </div>
</div>
<div class="container">
    <div class="breadcrumb">
        <a href="{$STORE_URL}">Home</a>
        <span class="fa fa-angle-double-right"></span>
        <span class="current">{$LANG.checkout.your_basket}</span>
    </div>
</div>
<!--breadcrumb-section ends-->

<section id="primary" class="content-full-width">
  <div class="container">
    <div class="checkout">
        {if isset($ITEMS)}
        <form action="{$VAL_SELF}" method="post" enctype="multipart/form-data" class="autosubmit" id="checkout_form">
           
           {if $INCLUDE_CHECKOUT}
           {include file='templates/content.checkout.confirm.php'}
           {/if}
           
           {include file='templates/content.checkout.cart.php'}
           
           <div class="coupon">
                <p><span>{$LANG.basket.coupon_add}</span><input name="coupon" id="coupon" type="text" maxlength="25"></p>
           </div>
           
           <div class="cart_buttons">
              <div class="empty_basket"><a href="{$STORE_URL}/index.php?_a=basket&empty-basket=true" class="checkout-button-light"><i class="fa fa-trash-o"></i> {$LANG.basket.basket_empty}</a></div>
              <div class="right">
                  <div class="update_basket "><button type="submit" name="update" value="{$LANG.basket.basket_update}" class="checkout-button-light"><i class="fa fa-refresh"></i> {$LANG.basket.basket_update}</button></div>
                  {if $DISABLE_CHECKOUT_BUTTON!==true}
                  <div class="checkout_proceed"><button type="submit" name="proceed" class="checkout-button-dark">{$CHECKOUT_BUTTON} <i class="fa fa-chevron-right"></i></button></div>
                  {/if}
              </div>
           </div>
        </form>
        
        {if $CHECKOUTS}
        <div class="express_checkouts">
           <p>-- {$LANG.common.or} --</p>
        	{foreach from=$CHECKOUTS item=checkout}
           	<div>{$checkout}</div>
        	{/foreach}
        </div>
        {/if}
        {if $RELATED}
        <div class="show-for-medium-up">
           <h2>{$LANG.catalogue.related_products}</h2>
           <ul class="small-block-grid-5 no-bullet">
              {foreach from=$RELATED item=product}
              <li>
                 <a href="{$product.url}" title="{$product.name}"><img src="{$product.img_src}" class="th" alt="{$product.name}"></a>
                 <br>
                 <a href="{$product.url}" title="{$product.name}">{$product.name}</a>
                 <p>
                    {if $product.ctrl_sale}
                    <span class="old_price">{$product.price}</span> <span class="sale_price">{$product.sale_price}</span>
                    {else}
                    {$product.price}
                    {/if}
                 </p>
              </li>
              {/foreach}
           </ul>
        </div>
        {/if}
        {else}
            <p>{$LANG.basket.basket_is_empty}</p>
       {/if}
     </div>
 </div>
</section>    