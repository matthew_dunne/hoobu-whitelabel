jQuery(document).ready(function($){
	
	load_ckeditor();

	google.maps.event.addDomListener(window, 'load', initializeMap);
	
	$('.fancybox').fancybox({
		type : "image"
	});
	
	$('#recent-products-slider').slick({
		autoplay: true,
		infinite: true,
		speed: 1000,
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
	});
	
});

function initializeMap() {
	var latLng = {lat: 35.048119, lng:-78.872325}
	var mapCanvas = document.getElementById('map');
	var mapOptions = {
	  center: latLng,
	  zoom: 18,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	var map = new google.maps.Map(mapCanvas, mapOptions);
	
	var marker = new google.maps.Marker({
    position: latLng,
    map: map,
  });
}

function load_ckeditor() {
	// We need to turn off the automatic editor creation first.
	CKEDITOR.disableAutoInline = true;

	var elements = jQuery( '.editable' );
	elements.each( function() {
		CKEDITOR.inline(this  ,{
		  on:{
			blur: function(event){
			  if (event.editor.checkDirty()) {

				  var data = event.editor.getData();
				  var id = this.element.$.id;
					
				  //$.post( "http://portal.andrewmichaels.co.uk/ws/v1/dynamic-content/ajax.php", { block_id: id, data: data }, function(){
	
				  //});
	
			  }
			}
		  }
		});
	});
}
